package sistema;

import java.time.LocalTime;
import java.util.Scanner;

import classes.Atendimento;
import classes.Paciente;
import tad.Fila;
import tad.Lista;

public class Main {
	public static void main(String[] args) {
		Scanner s = new Scanner(System.in);
		Fila fA = new Fila();
		Fila fA1 = new Fila();
		Fila fA2 = new Fila();
		Fila fA3 = new Fila();
		Fila fA4 = new Fila();
		Fila fA5 = new Fila();
		Lista lP = new Lista();
		Lista lAC = new Lista();
		boolean f = true;
		while (f) {
			String r;
			Atendimento a;
			Paciente p;
			Boolean flag;
			System.out.println("Selecione uma op��o:\n" + "1 - Cadastrar novo paciente\n"
					+ "2 - Pesquisar um paciente\n" + "3 - Iniciar atendimento de um paciente\n"
					+ "4 - Chamar o paciente para o processo de triagem\n" + "5 - Chamar o paciente para consulta\n"
					+ "6 - Realizar a libera��o do paciente\n" + "7 - Relat�rios Administrativos\n" + "0 - Sair\n");
			r = s.nextLine();
			switch (r) {
			case "0":
				f = false;
				break;
			case "1":
				cadP(lP);
				break;
			case "2":
				System.out.println("Insira o cpf para pesquisa:");
				r = s.nextLine();
				p = (Paciente) lP.searchByID(r);
				if (p != null)
					System.out.println(p.toString());
				else
					System.out.println("Paciente n�o encontrado!");
				break;
			case "3":
				System.out.println("Insira o cpf do paciente para iniciar o atendiemento:");
				flag = true;
				do {
					r = s.nextLine();
					if (!isNumeric(r))
						System.out.println("CPF inv�lido!");
					else
						flag = false;
				} while (flag);
				p = (Paciente) lP.searchByID(r);
				a = new Atendimento();
				if (p != null) {
					a.setPessoa(p);
					a.setHoraChegada(LocalTime.now());
					fA.enqueue(a);
					System.out.println("Paciente " + p.getNome() + " encanhimado a fila de espera!");
				} else {
					System.out.println("Paciente n�o encontrado!\nIniciando cadastro:");
					p = (Paciente) cadP(lP, r);
					a.setPessoa(p);
					a.setHoraChegada(LocalTime.now());
					fA.enqueue(a);
					System.out.println("Paciente " + p.getNome() + " encanhimado a fila de espera!");
				}
				break;
			case "4":
				a = (Atendimento) fA.dequeue();
				if (a != null) {
					System.out.println("Temperatura do paciente:");
					flag = true;
					do {
						r = s.nextLine();
						if (!isNumeric(r))
							System.out.println("Numero inv�lido!");
						else
							flag = false;
					} while (flag);
					a.setTemperatura(Double.parseDouble(r));
					System.out.println("Frequ�ncia card�aca do paciente:");
					flag = true;
					do {
						r = s.nextLine();
						if (!isNumeric(r))
							System.out.println("Numero inv�lido!");
						else
							flag = false;
					} while (flag);
					double fc = Double.parseDouble(r);
					System.out.println("Frequ�ncia respirat�ria do paciente:");
					flag = true;
					do {
						r = s.nextLine();
						if (!isNumeric(r))
							System.out.println("Numero inv�lido!");
						else
							flag = false;
					} while (flag);
					double fr = Double.parseDouble(r);
					System.out.println("Oximetria do pulso do paciente:");
					flag = true;
					do {
						r = s.nextLine();
						if (!isNumeric(r))
							System.out.println("Numero inv�lido!");
						else
							flag = false;
					} while (flag);
					double op = Double.parseDouble(r);
					System.out.println("Indice de pico do fluxo respiratorio do paciente:");
					flag = true;
					do {
						r = s.nextLine();
						if (!isNumeric(r))
							System.out.println("Numero inv�lido!");
						else
							flag = false;
					} while (flag);
					double iprf = Double.parseDouble(r);
					System.out.println("O paciente est� entubado, apn�tico, sem pulso ou sem rea��o?(sim/nao)");
					if (s.nextLine().equalsIgnoreCase("sim")) {
						System.out.println("Paciente " + a.getPessoa().getNome() + " com prioridade 1!");
						a.setPrioridade(1);
						fA1.enqueue(a);
					} else {
						System.out.println(
								"O paciente est� em uma situa��o de alto risco?confuso/let�rgico/desorientado? ou com dor/sofrimento agudo?(sim/nao)");
						if (s.nextLine().equalsIgnoreCase("sim")) {
							System.out.println("Paciente " + a.getPessoa().getNome() + " com prioridade 2!");
							a.setPrioridade(2);
							fA2.enqueue(a);
						} else {
							System.out.println(
									"O paciente necessita de quantos procediementos(como exames, inje��es, etc...):(muitas/uma/nenhuma)");
							r = s.nextLine();
							if (r.equalsIgnoreCase("muitas")) {
								if (a.getTemperatura() > 38 || a.getTemperatura() < 36 || fc > 90 || fr > 20 || op < 90
										|| iprf < 200) {
									System.out.println("Paciente " + a.getPessoa().getNome() + " com prioridade 2!");
									a.setPrioridade(2);
									fA2.enqueue(a);
								} else {
									System.out.println("Paciente " + a.getPessoa().getNome() + " com prioridade 3!");
									a.setPrioridade(3);
									fA3.enqueue(a);
								}
							} else if (r.equalsIgnoreCase("uma")) {
								System.out.println("Paciente " + a.getPessoa().getNome() + " com prioridade 4!");
								a.setPrioridade(4);
								fA4.enqueue(a);
							} else {
								System.out.println("Paciente " + a.getPessoa().getNome() + " com prioridade 5!");
								a.setPrioridade(5);
								fA5.enqueue(a);
							}
						}
					}
				} else
					System.out.println("N�o h� pacientes!");
				break;
			case "5":
				a = (Atendimento) fA1.dequeue();
				if (a != null) {
					a.setHoraAtendimento(LocalTime.now());
					lAC.addFinal(a);
					System.out.println("Paciente " + a.getPessoa().getNome() + " encaminhado para consulta!");
				} else {
					a = (Atendimento) fA2.dequeue();
					if (a != null) {
						a.setHoraAtendimento(LocalTime.now());
						lAC.addFinal(a);
						System.out.println("Paciente " + a.getPessoa().getNome() + " encaminhado para consulta!");
					} else {
						a = (Atendimento) fA3.dequeue();
						if (a != null) {
							a.setHoraAtendimento(LocalTime.now());
							lAC.addFinal(a);
							System.out.println("Paciente " + a.getPessoa().getNome() + " encaminhado para consulta!");
						} else {
							a = (Atendimento) fA4.dequeue();
							if (a != null) {
								a.setHoraAtendimento(LocalTime.now());
								lAC.addFinal(a);
								System.out
										.println("Paciente " + a.getPessoa().getNome() + " encaminhado para consulta!");
							} else {
								a = (Atendimento) fA5.dequeue();
								if (a != null) {
									a.setHoraAtendimento(LocalTime.now());
									lAC.addFinal(a);
									System.out.println(
											"Paciente " + a.getPessoa().getNome() + " encaminhado para consulta!");
								} else {
									System.out.println("N�o h� pacientes!");
								}
							}
						}
					}
				}
				break;
			case "6":
				System.out.println("Insira o cpf do paciente para libera-lo:");
				r = s.nextLine();
				p = (Paciente) lP.searchByID(r);
				a = (Atendimento) lAC.searchByID(p);
				if (a != null && a.getHoraSaida() == null) {
					System.out.println("Paciente " + a.getPessoa().getNome() + " liberado!");
					lAC.changeA(a.getPessoa());
				} else
					System.out.println("Paciente n�o cadastrado ou j� liberado!");
				break;
			case "7":
				System.out.println("Tempo m�dio de espera para atendimento:");
				System.out.printf("%.4f horas!\n", lAC.calcTMA());
				System.out.println("Tempo m�dio de atendimento:");
				System.out.printf("%.4f horas!\n", lAC.calcTME());
				System.out.println("Tempo m�dio de atendimento de cada fila:");
				System.out.println("Fila geral:");
				System.out.printf("%.4f horas!\n", lAC.calcTMAF());
				System.out.println("Fila de prioridade 1:");
				System.out.printf("%.4f horas!\n", lAC.calcTMAF(1));
				System.out.println("Fila de prioridade 2:");
				System.out.printf("%.4f horas!\n", lAC.calcTMAF(2));
				System.out.println("Fila de prioridade 3:");
				System.out.printf("%.4f horas!\n", lAC.calcTMAF(3));
				System.out.println("Fila de prioridade 4:");
				System.out.printf("%.4f horas!\n", lAC.calcTMAF(4));
				System.out.println("Fila de prioridade 5:");
				System.out.printf("%.4f horas!\n", lAC.calcTMAF(5));
				break;
			default:
				System.out.println("Comando inv�lido!");
				break;
			}
		}
	}

	/**
     * Insere o paciente na lista
     * 
     * @param lista de pacientes
     * @return o objeto paciente
     */
	public static Object cadP(Lista lP) {
		Scanner s = new Scanner(System.in);
		Paciente p = new Paciente();
		String res;
		System.out.println("Insira o nome do paciente:");
		p.setNome(s.nextLine());
		System.out.println("Insira o ano de nascimento do paciente:");
		boolean flag = true;
		;
		do {
			res = s.nextLine();
			if (isNumeric(res)) {
				p.setAnoNascimento(Integer.parseInt(res));
				flag = false;
			} else
				System.out.println("Ano inv�lido!");
		} while (flag);
		System.out.println("Insira o cpf do paciente:");
		flag = true;
		do {
			res = s.nextLine();
			if (lP.searchByID(res) != null || !isNumeric(res))
				System.out.println("CPF j� cadastrado ou inv�lido!");
			else
				flag = false;
		} while (flag);
		p.setCpf(res);
		lP.addInicio(p);
		return p;
	}
	
	/**
     * Insere o paciente na lista
     * 
     * @param lista de pacientes 
     * @param cpf
     * @return o objeto paciente
     */
	public static Object cadP(Lista lP, String r) {
		Scanner s = new Scanner(System.in);
		Paciente p = new Paciente();
		System.out.println("Insira o nome do paciente:");
		p.setNome(s.nextLine());
		System.out.println("Insira o ano de nascimento do paciente:");
		boolean flag = true;
		String res;
		do {
			res = s.nextLine();
			if (isNumeric(res)) {
				p.setAnoNascimento(Integer.parseInt(res));
				flag = false;
			} else
				System.out.println("Ano inv�lido!");
		} while (flag);
		p.setCpf(r);
		lP.addInicio(p);
		return p;
	}

	/**
     * Verifica se o dado informado � um numero
     * 
     * @param String
     * @return false caso nao seja numero e true caso seja um numero
     */
	public static boolean isNumeric(String str) {
		try {
			double d = Double.parseDouble(str);
		} catch (NumberFormatException nfe) {
			return false;
		}
		return true;
	}
}
