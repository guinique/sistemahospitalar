package classes;

import java.time.LocalTime;

public class Atendimento {
	private Paciente pessoa;
	private LocalTime horaChegada;
	private LocalTime horaAtendimento;
	private LocalTime horaSaida;
	private double temperatura;
	private int prioridade;
	private String parecer;
	
	public Atendimento() {
	}

	public Atendimento(Paciente pessoa, LocalTime horaChegada, LocalTime horaAtendimento, LocalTime horaSaida, double temperatura,
			int prioridade, String parecer) {
		super();
		this.pessoa = pessoa;
		this.horaChegada = horaChegada;
		this.horaAtendimento = horaAtendimento;
		this.horaSaida = horaSaida;
		this.temperatura = temperatura;
		this.prioridade = prioridade;
		this.parecer = parecer;
	}

	public Paciente getPessoa() {
		return pessoa;
	}

	public void setPessoa(Paciente pessoa) {
		this.pessoa = pessoa;
	}

	public LocalTime getHoraChegada() {
		return horaChegada;
	}

	public void setHoraChegada(LocalTime horaChegada) {
		this.horaChegada = horaChegada;
	}

	public LocalTime getHoraAtendimento() {
		return horaAtendimento;
	}

	public void setHoraAtendimento(LocalTime horaAtendimento) {
		this.horaAtendimento = horaAtendimento;
	}

	public LocalTime getHoraSaida() {
		return horaSaida;
	}

	public void setHoraSaida(LocalTime horaSaida) {
		this.horaSaida = horaSaida;
	}

	public double getTemperatura() {
		return temperatura;
	}

	public void setTemperatura(double temperatura) {
		this.temperatura = temperatura;
	}

	public int getPrioridade() {
		return prioridade;
	}

	public void setPrioridade(int prioridade) {
		this.prioridade = prioridade;
	}

	public String getParecer() {
		return parecer;
	}

	public void setParecer(String parecer) {
		this.parecer = parecer;
	}

	@Override
	public String toString() {
		return "Atendimento [pessoa=" + pessoa + ", horaChegada=" + horaChegada + ", horaAtendimento=" + horaAtendimento
				+ ", horaSaida=" + horaSaida + ", temperatura=" + temperatura + ", prioridade=" + prioridade
				+ ", parecer=" + parecer + "]";
	}
	
}
