package tad;

import classes.Atendimento;

public class Fila {
	private Nodo primeiro;
	private Nodo ultimo;

	public Fila() {
		this.primeiro = null;
		this.primeiro = null;
	}
	
	
	/**
     * Adiciona um novo dado na fila
     * 
     * @param objeto dado
     */
	public void enqueue(Object dado) {
		Nodo novoNodo = new Nodo(dado);
		if(primeiro==null) {
			primeiro = novoNodo;
			ultimo = novoNodo;
		}else {
			ultimo.setProximo(novoNodo);
			ultimo = novoNodo;
		}
	}
	
	/**
     * Remove um elemento da fila
     * 
     * @return retorna o elemento removido
     */
	public Object dequeue() {
		Atendimento a = null;
		if(primeiro == null) {
			return null;
		}else {
			a = (Atendimento) primeiro.getDado();
			primeiro = primeiro.getProximo();
		}
		return a;
	}
	
	/**
     * Retorna o elemento do inicio da fila
     * 
     * @return o primeiro elemento
     */
	public Object head() {
		if(primeiro == null) {
			return null;
		}else {
			return primeiro.getDado();
		}
		
	}
	
	/**
     * Retorna o tamanho da fila
     * 
     * @return o tamanho da fila
     */
	public int size() {
		Nodo aux = primeiro;
		int cont = 0;
		while(aux.getProximo()!=null) {
			cont++;
			aux = aux.getProximo();
		}
		cont++;
		return cont;
	}
	
	/**
     * Retorna true se a fila esta vazia
     * 
     * @return true ou false case esteja vazia
     */
	public boolean isEmpty() {
		if(primeiro == null) {
			return true;
		}
		return false;
	}
	
	/**
     * Remove todos elementos da fila
     */
	public void clear() {
		primeiro = null;
	}
	
	/**
     * Imprime os dados da fila
     */
	public void print() {
		Nodo aux = primeiro;
		
		while(aux.getProximo()!=null) {
			System.out.println(aux.getDado().toString());
			aux = aux.getProximo();
		}
		System.out.println(aux.getDado());
	}
}
