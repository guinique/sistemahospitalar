package tad;
import java.time.LocalTime;

import classes.Atendimento;
import classes.Paciente;

public class Lista {
    private Nodo primeiro;
    
    public Lista(){        
        this.primeiro = null;
    }

    /**
     * Adiciona um novo elemento ao final da lista
     * 
     * @param dado String conte�do a ser adicionado
     */
    public void addFinal(Object dado){
        if(primeiro == null){
            primeiro = new Nodo(dado);
        } else {        
            Nodo novoNodo = new Nodo(dado);
            Nodo aux = primeiro;
            while(aux.getProximo() != null){
                aux = aux.getProximo();
            }
            aux.setProximo(novoNodo);  
        }
    }
    
    /**
     * Adiciona um novo elemento ao inicio da lista
     * 
     * @param dado String conte�do a ser adicionado
     */
    public void addInicio(Object dado) {
    	if(primeiro == null) {
    		primeiro = new Nodo(dado);
    	}else {
    		Nodo novoNodo = new Nodo(dado);
    		novoNodo.setProximo(primeiro);
    		primeiro = novoNodo;
    	}
    }
    
    /**
     * Remove um elemento de determinada poi��o da fila
     * 
     * @param posi��o do dado a ser removido
     */
    public void removePos(int pos){        
        Nodo aux = primeiro;  
        int cont = 0;
        if(pos == 0){
            primeiro = primeiro.getProximo();
            cont++;
        } else {
            do{
                if(cont == pos){
                    aux.setProximo(aux.getProximo().getProximo());
                } else {
                    aux = aux.getProximo();
                    cont++;
                }
            }while(aux != null);                
        }
    }
    
    /**
     * Procura um objeto na lista pela posi��o
     * 
     * @param posi��o do objeto
     * @return o objeto caso seja econtrado ou null caso nao seja
     */
    public Object searchByPosition(int pos) {
    	Nodo aux = primeiro;
        if(aux!=null) {
        	int c = 0;
        	do {
    			if (c == pos) {
					return aux.getDado();
				}else {
                    aux = aux.getProximo();
                    c++;
                }
    		}while(aux!=null);
        }
        return null;
    }
    
    /**
     * Procura um objeto na lista pela id
     * 
     * @param id do objeto
     * @return o objeto caso seja econtrado ou null caso nao seja
     */
    public Object searchByID (String id) {
    	Nodo aux = primeiro;
    	if(aux != null) {
    		do {
    			Paciente p = (Paciente) aux.getDado();
    			if (p.getCpf().equals(id)) {
					return p;
				}else {
                    aux = aux.getProximo();
                }
    		}while(aux!=null);
    	}
    	return null;
    }
    
    /**
     * Procura um objeto na lista pela id
     * 
     * @param id do objeto
     * @return o objeto caso seja econtrado ou null caso nao seja
     */
    public Object searchByID (Paciente p) {
    	Nodo aux = primeiro;
    	if(aux != null) {
    		do {
    			Atendimento a = (Atendimento) aux.getDado();
    			if (a.getPessoa().getCpf().equals(p.getCpf()) && a.getHoraSaida()==null) {
					return a;
				}else {
                    aux = aux.getProximo();
                }
    		}while(aux!=null);
    	}
    	return null;
    }
    
    /**
     * Define o horario de sa�da do paciente
     * 
     * @param objeto Paciente
     */
    public void changeA (Paciente p) {
    	Nodo aux = primeiro;
    	if(aux != null) {
    		do {
    			Atendimento a = (Atendimento) aux.getDado();
    			if (a.getPessoa().getCpf().equals(p.getCpf())) {
					a.setHoraSaida(LocalTime.now());
					aux.setDado(a);
					break;
				}else {
                    aux = aux.getProximo();
                }
    		}while(aux!=null);
    	}
    }
    
    /**
     * Retorna o tempo medio de entrada
     * 
     * @return o tempo medio
     */
    public double calcTME() {
		Nodo aux = primeiro;
		double d = 0;
		int x = 0;
		if(aux != null) {
			do {
				Atendimento a = (Atendimento) aux.getDado();
				double aux1 = (double)a.getHoraAtendimento().getMinute()/60;
				aux1 -= (double)a.getHoraChegada().getMinute()/60;
				Math.abs(aux1);
				double aux2 = (double)a.getHoraAtendimento().getHour();
				aux2 -= (double)a.getHoraChegada().getHour();
				Math.abs(aux2);
				d += aux1;
				d += aux2;
				x++;
				aux = aux.getProximo();
			}while(aux!=null);
		}else
			return 0;
		return d/x;
	}
    
    /**
     * Retorna o tempo medio de espera de atendimento
     * 
     * @return o tempo medio
     */
    public double calcTMA() {
		Nodo aux = primeiro;
		double d = 0;
		int x = 0;
		if(aux != null) {
			do {
				Atendimento a = (Atendimento) aux.getDado();
				double aux1 = (double)a.getHoraSaida().getMinute()/60;
				aux1 -= (double)a.getHoraAtendimento().getMinute()/60;
				Math.abs(aux1);
				double aux2 = (double)a.getHoraSaida().getHour();
				aux2 -= (double)a.getHoraAtendimento().getHour();
				Math.abs(aux2);
				d += aux1;
				d += aux2;
				x++;
				aux = aux.getProximo();
			}while(aux!=null);
		}else
			return 0;
		return d/x;
	}
    
    /**
     * Retorna o tempo medio de fila
     * 
     * @return o tempo medio
     */
    public double calcTMAF() {
		Nodo aux = primeiro;
		double d = 0;
		int x = 0;
		if(aux != null) {
			do {
				Atendimento a = (Atendimento) aux.getDado();
				double aux1 = (double)a.getHoraSaida().getMinute()/60;
				aux1 -= (double)a.getHoraChegada().getMinute()/60;
				Math.abs(aux1);
				double aux2 =(double)a.getHoraSaida().getHour();
				aux2 -= (double)a.getHoraChegada().getHour();
				Math.abs(aux1);
				d += aux1;
				d += aux2;
				x++;
				aux = aux.getProximo();
			}while(aux!=null);
		}else
			return 0;
		return d/x;
	}
    /**
     * Retorna o tempo medio de uma fila de prioridade especifica
     * 
     * @return o tempo medio
     */
    public double calcTMAF(int p) {
		Nodo aux = primeiro;
		double d = 0;
		int x = 0;
		if(aux != null) {
			do {
				Atendimento a = (Atendimento) aux.getDado();
				if(a.getPrioridade()==p){
					double aux1 = (double)a.getHoraSaida().getMinute()/60;
					aux1 -= (double)a.getHoraChegada().getMinute()/60;
					Math.abs(aux1);
					double aux2 =(double)a.getHoraSaida().getHour();
					aux2 -= (double)a.getHoraChegada().getHour();
					Math.abs(aux1);
					d += aux1;
					d += aux2;
					x++;
				}
				aux = aux.getProximo();
			}while(aux!=null);
		}else
			return 0;
		return d/x;
	}
    /**
     * Imprime os dados da lista
     */
    public void imprimir(){
        Nodo aux = primeiro;        
        while(aux.getProximo()!=null){
            System.out.println(aux.getDado().toString());
            aux=aux.getProximo();
        }
        System.out.println(aux.getDado().toString());   
    }
}