package tad;
public class Nodo {
	private Object dado;
	private Nodo proximo;
	
	public Nodo(Object dado) {
		this.dado = dado;
		this.proximo = null;
	}
	
	public Object getDado() {
		return dado;
	}
	public void setDado(Object dado) {
		this.dado = dado;
	}
	public Nodo getProximo() {
		return proximo;
	}
	public void setProximo(Nodo proximo) {
		this.proximo = proximo;
	}
}
